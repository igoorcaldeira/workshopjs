# API IMDB
## Objetivo
Criar uma api REST que fará uma integração com a api do [IMDB](https://developers.themoviedb.org/3/getting-started/introduction)

## Regras
 - Todo o time trabalhará junto aplicando o conhecimento adquirido no workshop
 - Cada um escolherá uma feature para desenvolver
 - No final todos irão criar PRs para esse repositório os quais será analisado sintaxe de código e o funcionamento da API
 - Obrigatoriamente a API deverá ser desenvolvida utilizando Javascript e utilizar o Git flow para controle de fluxo das features
 - Vocês terão apenas o tempo do workshop para desenvolver a API
 - A estruturação do projeto estará em MVC e não será necessário a utilização de algum banco
 - O time deverá implementar no mínimo 3 features e será de escolha do próprio time
 - O time deverá fornecer a collection e o environment do postman para que a API possa ser testada.

## Informações complementares
 - O token da API está no arquivo `.env` na raiz do projeto.
 - A lib para HTTP request está pré-configurada em `src/lib/api`